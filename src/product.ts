import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepositpry = AppDataSource.getRepository(Product)

    const products = await productRepositpry.find()
    console.log("Loaded product: ", products)

    const updateProduct = await productRepositpry.findOneBy({id: 1})
    console.log(updateProduct)
    updateProduct.price = 80
    await productRepositpry.save(updateProduct)

}).catch(error => console.log(error))
